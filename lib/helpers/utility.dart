import 'package:intl/intl.dart';

enum CalendarRange {
  Today,
  PreviousDay,
  CurrentWeek,
  PreviousWeek,
  CurrentMonth,
  PreviousMonth,
  CurrentFinancialYear,
  PreviousFinancialYear
}

class Utility {
  static String getInitials(String text) {
    var aText = text.trim().split(" ");
    String initials = "";
    for (var i = 0; i < aText.length; i++) {
      if (i > 1) break;
      try {
        initials += aText[i].substring(0, 1);
      } on Exception catch (e) {
        print(e.toString());
      }
    }
    return initials;
  }

  static String formatDateTime(String dateTime) {
    var formatter = new DateFormat('MMM dd, yyyy\nHH:mm a');
    var formatter2 = new DateFormat('HH:mm a');
    DateTime dt = DateTime.parse(dateTime);
    if (dt.day == DateTime.now().day)
      return formatter2.format(dt);
    else
      return formatter.format(dt);
  }

  static String formatToDateYMD(String dateTime) {
    DateTime dt = DateTime.parse(dateTime);
    var formatter = new DateFormat('yyyy-MM-dd');
    return formatter.format(dt);
  }

  static String formatToDateDMY(String dateTime) {
    DateTime dt = DateTime.parse(dateTime);
    var formatter = new DateFormat('dd MMM\nyyyy');
    return formatter.format(dt);
  }

  static String formatToDateDM(String dateTime) {
    DateTime dt = DateTime.parse(dateTime);
    var formatter = new DateFormat('dd\nMMM');
    return formatter.format(dt);
  }
}
