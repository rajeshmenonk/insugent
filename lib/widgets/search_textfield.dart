import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:insugent/constants.dart';

class SearchTexfield extends StatefulWidget {
  SearchTexfield({
    Key? key,
    required this.searchController,
    required this.onSearch,
    required this.onClearSearch,
    this.enableDateSelection,
    this.onDateSelectorTap,
  }) : super(key: key);
  final TextEditingController searchController;
  final Function onSearch;
  final Function onClearSearch;
  final bool? enableDateSelection;
  final Function? onDateSelectorTap;
  @override
  State<StatefulWidget> createState() {
    return _SearchTextfieldState();
  }
}

class _SearchTextfieldState extends State<SearchTexfield> {
  bool hasValue = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var brightness = SchedulerBinding.instance!.window.platformBrightness;
    bool darkModeOn = brightness == Brightness.dark;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        padding: kGlobalOuterPadding,
        decoration: BoxDecoration(
            // border: Border.all(color: Colors.black12),
            borderRadius: BorderRadius.circular(25),
            // color: kAccentColor.withOpacity(0.2),
            color: darkModeOn ? Colors.black26 : Colors.grey[100]),
        child: Row(
          children: [
            Icon(CupertinoIcons.search),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: TextField(
                controller: widget.searchController,
                decoration: InputDecoration.collapsed(
                  hintText: 'Search',
                  hintStyle: TextStyle(fontWeight: FontWeight.w600),
                ),
                onEditingComplete: () => widget.onSearch(),
                autofocus: false,
                onChanged: (value) {
                  setState(() {
                    hasValue = (value.isNotEmpty);
                  });
                },
              ),
            ),
            SizedBox(
              width: 5,
            ),
            Visibility(
              visible: hasValue,
              child: InkWell(
                borderRadius: BorderRadius.circular(8.0),
                onTap: () => widget.onClearSearch(),
                child: Icon(Icons.clear_outlined),
              ),
            ),
            SizedBox(
              width: 5,
            ),
            Visibility(
              visible: (widget.enableDateSelection ?? false) &&
                  widget.enableDateSelection != null,
              child: InkWell(
                borderRadius: BorderRadius.circular(8.0),
                onTap: () => widget.onDateSelectorTap!(),
                child: Icon(Icons.filter_list_outlined),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
