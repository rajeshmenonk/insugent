import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextField extends StatefulWidget {
  final String hint;
  final Icon? icon;
  final TextEditingController controller;
  final TextInputType? inputType;
  final int? maxLength;
  final bool? obscureText;
  const CustomTextField(
      {Key? key,
      required this.hint,
      this.icon,
      required this.controller,
      this.inputType,
      this.maxLength,
      this.obscureText})
      : super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    var brightness = MediaQuery.of(context).platformBrightness;
    bool darkModeOn = brightness == Brightness.dark;
    return Container(
        padding: const EdgeInsets.all(15.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15.0),
            color: darkModeOn ? Colors.black26 : Colors.grey[100]),
        child: Row(
          children: [
            widget.icon ?? Container(),
            Visibility(
              child: SizedBox(
                width: 8,
              ),
              visible: widget.icon != null,
            ),
            Expanded(
              child: TextField(
                controller: widget.controller,
                keyboardType: widget.inputType,
                obscureText: widget.obscureText ?? false,
                maxLength: widget.maxLength,
                decoration: InputDecoration.collapsed(
                  hintText: widget.hint,
                ),
              ),
            ),
          ],
        ));
  }
}
