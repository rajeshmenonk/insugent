import 'package:flutter/material.dart';
import 'package:insugent/widgets/custom_textfield.dart';

import '../constants.dart';
import 'login.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _fullNameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: 300,
                  padding: const EdgeInsets.only(top: 50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child: Image.asset(
                          'res/insugent.png',
                          width: 40.0,
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        kAppName,
                        style: TextStyle(
                            fontSize: 24.0, fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: kGlobalOuterPadding,
                  width: 300,
                  child: CustomTextField(
                    controller: _emailController,
                    hint: 'Your E-Mail',
                    icon: Icon(Icons.email_outlined),
                    inputType: TextInputType.emailAddress,
                  ),
                ),
                Container(
                  padding: kGlobalOuterPadding,
                  width: 300,
                  child: CustomTextField(
                    controller: _fullNameController,
                    hint: 'Full Name',
                    icon: Icon(Icons.person_outline),
                    inputType: TextInputType.emailAddress,
                  ),
                ),
                Container(
                  padding: kGlobalOuterPadding,
                  width: 300,
                  child: CustomTextField(
                    obscureText: true,
                    controller: _passwordController,
                    hint: 'Create Password',
                    icon: Icon(Icons.password_outlined),
                  ),
                ),
                Container(
                  width: 300,
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextButton(
                        onPressed: () => Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(builder: (context) => Login()),
                        ),
                        child: Text('Sign-In'),
                      ),
                      ElevatedButton(
                        onPressed: () {},
                        child: Text('Sign-Up'),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('©2021 RSoft'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
