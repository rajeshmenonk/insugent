import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF15a699);
const kAccentColor = Color(0xFFd7726f);
const kSecondaryColor = Color(0xFF16a7A0);
const kTextColor = Color(0xFF21222D);
const kBorderColor = Color(0xFF757575);
const kAlertColor = Colors.red;

const kAppName = 'Insugent';
const kBaseUrl = "http://insugent.knoxxbox.in/";
const kPageSize = 30;

const kGlobalOuterPadding = EdgeInsets.all(10.0);
const kGlobalCardPadding = EdgeInsets.all(5.0);
const kGlobalTextPadding =
    EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0);
