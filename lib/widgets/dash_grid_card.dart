import 'package:flutter/material.dart';

class DashGridCard extends StatefulWidget {
  final String leadText;
  final String bottomText;
  final Color backGroundColor;
  final Function onTap;
  const DashGridCard(
      {Key? key,
      required this.leadText,
      required this.bottomText,
      required this.backGroundColor,
      required this.onTap})
      : super(key: key);

  @override
  _DashGridCardState createState() => _DashGridCardState();
}

class _DashGridCardState extends State<DashGridCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: widget.backGroundColor.withOpacity(0.2),
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: InkWell(
        borderRadius: BorderRadius.circular(15.0),
        onTap: () => widget.onTap(),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.leadText,
                style: TextStyle(
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Quicksand'),
              ),
              Spacer(
                flex: 1,
              ),
              Container(
                child: Text(widget.bottomText),
                alignment: Alignment.centerRight,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
