import 'package:flutter/material.dart';

import '../constants.dart';

class BottomSheetContainer extends StatefulWidget {
  final Widget child;
  final bool isDesktop;
  BottomSheetContainer(
      {Key? key, required this.child, required this.isDesktop});
  @override
  _BottomSheetContainerState createState() => _BottomSheetContainerState();
}

class _BottomSheetContainerState extends State<BottomSheetContainer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomCenter,
      child: Container(
        margin: EdgeInsets.only(top: 15.0),
        padding: kGlobalOuterPadding,
        width: widget.isDesktop ? 600 : MediaQuery.of(context).size.width,
        child: widget.child,
      ),
    );
  }
}
