import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:flutter/services.dart';
// ignore: unused_import
import "package:http/http.dart" as http;
// ignore: unused_import
import 'package:insugent/pages/home.dart';
import 'package:insugent/pages/signup_page.dart';
import 'package:insugent/widgets/custom_textfield.dart';
import 'package:shared_preferences/shared_preferences.dart';
// ignore: unused_import
import 'package:uuid/uuid.dart';
import '../constants.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late SharedPreferences sharedPreferences;
  TextEditingController _usernameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  bool isLoading = false;
  String userId = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 50),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: Image.asset(
                      'res/insugent.png',
                      width: 80.0,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    kAppName,
                    style:
                        TextStyle(fontSize: 24.0, fontWeight: FontWeight.w300),
                  ),
                ),
                Container(
                  padding: kGlobalOuterPadding,
                  width: 300,
                  child: CustomTextField(
                    controller: _usernameController,
                    hint: 'E-Mail',
                    icon: Icon(Icons.person_outline),
                  ),
                ),
                Container(
                  padding: kGlobalOuterPadding,
                  width: 300,
                  child: CustomTextField(
                    obscureText: true,
                    controller: _passwordController,
                    hint: 'Password',
                    icon: Icon(Icons.password_outlined),
                  ),
                ),
                Container(
                  width: 300,
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextButton(
                        onPressed: () => Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(builder: (context) => SignUpPage()),
                        ),
                        child: Text('Sign-Up'),
                      ),
                      ElevatedButton(
                        onPressed: () {},
                        child: Text('Sign-In'),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('©2021 RSoft'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
